# Interview Questions for Applicants to the vacant innoCampus Job

Take your time, do not hurry, do not worry. It is not important that you solve every question perfectly. You can also hand in sketched answers or partly answers.

## Question 1 (Optimize our Program)
Given the following datastructure:
  ```
    {
        "scores": [
            {
                "uxid": "OBXP1",
                "maxpoints": 4,
                "points": 0,
                "siteuxid": "VBKM03_QuadratischeUngleichungen",
                "section": 3,
                "id": "QFELD_3.3.3.QF1",
                "intest": false,
                "value": 0,
                "rawinput": "",
            },
            {
                "uxid": "PXL",
                "maxpoints": 4,
                "points": 0,
                "siteuxid": "VBKM03_QuadratischeUngleichungen",
                "section": 3,
                "id": "QFELD_3.3.3.QF2",
                "intest": false,
                "value": 0,
                "rawinput": "",
            }
            // ... many more score entries here ...
        ],
        "login": {
            "type": 2,
            "vname": "Michi",
            "sname": "Muster",
            "username": "MichiM",
            "password": "31c§csa",
            "email": "michim@tu-berlin.de",
            "variant": "std",
            "sgang": "Informatik",
            "uni": "TU-Berlin"
        }
    }
  ```

  The datastructure stores the user's data that is saved in the browser's localstorage when they use our website. The access of "scores" happens quite often, on every subpage, always when the user changes an input field (which there are many of). Input Fields are accessed and identified by the "uxid" attribute above.
  
  **What would you change in the data structure to improve the website and why?**

## Question 2 (Authentication)
Sketch how you would program an authentication solution for websites.
You can, if you want, consider different possible options.

## Question 3 (App Architecture)
Our goal is to have native mobile apps for the converted courses. How
would you go about writing an app for the project? 

Note: The course content (including questions) is updated regularly and should not require the users to download a new app.

## Question 4 (Algorithm + Python)
Given an array a that contains only numbers in the range from 1 to
a.length, find the first duplicate number for which the second
occurrence has the minimal index. In other words, if there are more than
1 duplicated numbers, return the number for which the second occurrence
has a smaller index than the second occurrence of the other number does.
If there are no such elements, return -1.

Note: Write a solution with computing time and space in mind.


### Example:

For `a = [2, 3, 3, 1, 5, 2]`, the output should be
`firstDuplicate(a) = 3`

There are 2 duplicates: numbers 2 and 3. The second occurrence of 3 has a smaller index than than second occurrence of 2 does, so the answer is 3.

For `a = [2, 4, 3, 5, 1]`, the output should be
`firstDuplicate(a) = -1`


